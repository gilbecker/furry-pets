package com.wakaw.randomcat

import android.content.Context
import android.os.Bundle
import android.view.View
import com.wakaw.randomcat.data.FurryPet

interface MainContract {

    interface MainView {
        fun showImage(imageUrl: String?, isDog: Boolean)
        fun clearImage()
        fun startButtonAnimation(view: View)
        fun startImageAnimation(view: View)
        fun playSound (sound: Int)
        fun initConfetti()
        fun changeLikeButtonState(result: Boolean?)
    }

    interface MainPresenter {
        fun onCatButtonClicked(view: View)
        fun onDogButtonClicked(view: View)
        fun onMainImageClicked(view: View)
        fun attachView (view: MainContract.MainView)
        fun onViewRestored(lastImage: String?)
        fun detachView()
        fun onLikedClicked(view: View)
        fun celebrateCreation()
        fun getLastImage()
    }
}

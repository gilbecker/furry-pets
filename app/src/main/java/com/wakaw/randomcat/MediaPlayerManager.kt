package com.wakaw.randomcat

import android.media.MediaPlayer

class MediaPlayerManager {

    private var maxStreams: Int = 10
    private val playersInUse = mutableListOf<MediaPlayer>()
    private val mediaPlayerPool = mutableListOf<MediaPlayer>().also {
        for (i in 0..maxStreams) it += buildPlayer()
    }

    private fun buildPlayer() = MediaPlayer().apply {
        setOnPreparedListener {
            start()
        }
        setOnCompletionListener {
            recyclePlayer(it)
        }
    }

    /**
     * Returns a [MediaPlayer] if one is available,
     * otherwise null.
     */
    fun requestPlayer(): MediaPlayer? {
        return if (!mediaPlayerPool.isEmpty()) {
            mediaPlayerPool.removeAt(0).also {
                playersInUse += it
            }
        } else null
    }

    private fun recyclePlayer(mediaPlayer: MediaPlayer) {
        mediaPlayer.reset()
        playersInUse -= mediaPlayer
        mediaPlayerPool += mediaPlayer
    }

}
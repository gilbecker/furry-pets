package com.wakaw.randomcat.favorites

import android.content.Context
import com.wakaw.randomcat.data.DataInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class FavoritesPresenter(private var view: FavoritePetsContract.FavoritePetsView, private var dataInteractor: DataInteractor) : FavoritePetsContract.FavoritePetsPresenter {

    override fun getFavoritePets(context: Context) {

        val sub = dataInteractor.getAllFavoritePets().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe { result ->
                val adapter = FavoritePetsAdapter(result, context)
                view.showFavoritePetsInRecyclerView(adapter)
            }
    }

}
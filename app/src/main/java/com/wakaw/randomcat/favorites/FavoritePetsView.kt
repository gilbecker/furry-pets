package com.wakaw.randomcat.favorites

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.widget.LinearLayout
import com.wakaw.randomcat.R
import com.wakaw.randomcat.data.DataInteractor
import kotlinx.android.synthetic.main.activity_favorite_pets_view.*

class FavoritePetsView : AppCompatActivity(), FavoritePetsContract.FavoritePetsView {

    private lateinit var presenter: FavoritesPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite_pets_view)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        presenter = FavoritesPresenter(this, DataInteractor(this))

        presenter.getFavoritePets(this)
    }

    override fun showFavoritePetsInRecyclerView(adapter: FavoritePetsAdapter) {
        favoriteRecyclerView.layoutManager = GridLayoutManager(this, 3, LinearLayout.VERTICAL, false)
        favoriteRecyclerView.adapter = adapter
    }

}

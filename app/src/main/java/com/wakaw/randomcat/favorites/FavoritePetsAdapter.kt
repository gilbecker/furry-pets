package com.wakaw.randomcat.favorites

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.wakaw.randomcat.R
import com.wakaw.randomcat.data.FurryPet
import kotlinx.android.synthetic.main.item_favorite_pet.view.*

class FavoritePetsAdapter(private val favoritePets: List<FurryPet>, val context: Context) :
    RecyclerView.Adapter<FavoritePetsAdapter.ViewItemHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewItemHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_favorite_pet, parent, false)
        return ViewItemHolder(view)
    }

    override fun getItemCount(): Int {
        return favoritePets.size
    }

    override fun onBindViewHolder(itemHolder: ViewItemHolder, position: Int) {
        val pet = favoritePets[position]

        Glide.with(context).load(pet.url)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(itemHolder.favoritePetIV)
    }

    class ViewItemHolder(view: View) : RecyclerView.ViewHolder(view) {
        val favoritePetIV: ImageView = view.favoritePetIV
    }
}
package com.wakaw.randomcat.favorites

import android.content.Context
import android.view.View
import com.wakaw.randomcat.MainContract
import com.wakaw.randomcat.data.FurryPet


interface FavoritePetsContract {

    interface FavoritePetsView {
        fun showFavoritePetsInRecyclerView(adapter: FavoritePetsAdapter)
    }

    interface FavoritePetsPresenter {
        fun getFavoritePets(context: Context)
    }
}

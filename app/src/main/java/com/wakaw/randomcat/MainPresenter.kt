package com.wakaw.randomcat

import android.view.View
import android.widget.ToggleButton
import com.wakaw.randomcat.data.DataInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MainPresenter(view: MainContract.MainView, private var dataInteractor: DataInteractor) : MainContract.MainPresenter {

    private var mView: MainContract.MainView? = view
    private var isDog: Boolean = false
    private var disposables: CompositeDisposable? = CompositeDisposable()
    private var currentSubscription: Disposable? = null

    override fun onCatButtonClicked(view: View) {
        mView?.clearImage()
        mView?.startButtonAnimation(view)
        isDog = false

        if (currentSubscription != null) {
            currentSubscription?.dispose()
//            dataInteractor.cancelRequests()
        }

        currentSubscription =
                dataInteractor.getRandomCatImage().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe { result ->
                        mView?.showImage(result, isDog)
                    }

        val sub = dataInteractor.isPetExists().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe { result ->
                mView?.changeLikeButtonState(result)
            }

//        disposables?.add(currentSubscription)
    }

    override fun onDogButtonClicked(view: View) {
        mView?.clearImage()
        mView?.startButtonAnimation(view)
        isDog = true

        if (currentSubscription != null) {
            currentSubscription?.dispose()
//            dataInteractor.cancelRequests()
        }

        currentSubscription =
                dataInteractor.getRandomDogImage().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe { result ->
                        mView?.showImage(result, isDog)
                    }

        val sub = dataInteractor.isPetExists().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe { result ->
                mView?.changeLikeButtonState(result)
            }
    }

    override fun onMainImageClicked(view: View) {
        mView?.startImageAnimation(view)
        mView?.playSound(dataInteractor.getRandomSound(isDog))
    }

    override fun celebrateCreation() {
        mView?.initConfetti()
    }

    override fun onViewRestored(lastImage: String?) {
        mView?.showImage(lastImage, false)
    }

    override fun onLikedClicked(view: View) {
        if ((view as ToggleButton).isChecked) {
            dataInteractor.insertPet()
        } else {
            dataInteractor.deletePet()
        }

        mView?.startImageAnimation(view)
    }

    override fun attachView(view: MainContract.MainView) {
        this.mView = view
        dataInteractor.cacheFirstBunchOfImages()
    }

    override fun getLastImage() {
        val lastImage = dataInteractor.getLastImage()
        if (!lastImage.equals("")) {
            mView?.showImage(lastImage, true)
        }
    }

    override fun detachView() {
//        dataInteractor.cancelRequests()
        dataInteractor.saveLastImage()
        this.mView = null
        currentSubscription?.dispose()
    }
}
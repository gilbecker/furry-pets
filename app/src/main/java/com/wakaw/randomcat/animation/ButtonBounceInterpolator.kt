package com.wakaw.randomcat.animation

class ButtonBounceInterpolator : android.view.animation.Interpolator {

    private var mAmplitude: Double = 0.1
    private var mFrequency: Double = 22.0

    override fun getInterpolation(p0: Float): Float {
        return (-1 * Math.pow(Math.E, -p0 / mAmplitude) * Math.cos(mFrequency * p0) + 1).toFloat()
    }
}
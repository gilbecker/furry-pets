package com.wakaw.randomcat.animation

class ImageBounceInterpolator : android.view.animation.Interpolator {

    private var mAmplitude: Double = 0.07
    private var mFrequency: Double = 15.0

    override fun getInterpolation(p0: Float): Float {
        return (-1 * Math.pow(Math.E, -p0 / mAmplitude) * Math.cos(mFrequency * p0) + 1).toFloat()
    }
}
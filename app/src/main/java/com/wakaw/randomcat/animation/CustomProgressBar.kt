package com.wakaw.randomcat.animation

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v4.widget.CircularProgressDrawable
import com.wakaw.randomcat.R

class CustomProgressBar(context: Context, isDog: Boolean) : CircularProgressDrawable(context) {

    init {
        strokeWidth = 20f
        centerRadius = 40f

        if (isDog) {
            setColorSchemeColors(ContextCompat.getColor(context, R.color.colorAccent))
        } else {
            setColorSchemeColors(ContextCompat.getColor(context, R.color.colorPrimary))
        }

        start()
    }
}
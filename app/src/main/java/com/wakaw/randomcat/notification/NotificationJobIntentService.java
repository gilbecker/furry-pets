package com.wakaw.randomcat.notification;

import android.annotation.TargetApi;
import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.JobIntentService;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.wakaw.randomcat.MainActivity;
import com.wakaw.randomcat.R;
import com.wakaw.randomcat.data.CatApiService;
import com.wakaw.randomcat.data.DataInteractor;
import com.wakaw.randomcat.data.DogApiService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class NotificationJobIntentService extends JobIntentService {

    public static final String CHANNEL_ID = "666";

    private Disposable disposable;

    private Context mContext;

    public static void startWork(Context context) {
        enqueueWork(context, NotificationJobIntentService.class, 666, new Intent());
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        createChannel();

        DataInteractor dataInteractor = new DataInteractor(this, CatApiService.Companion.create(), DogApiService.Companion.create());

        mContext = this;

        // try to subscribe on current thread - VVV
        // use JobService (block operations)
        disposable = dataInteractor.getRandomDogImage().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) {
                Intent i = new Intent(mContext, MainActivity.class);
                i.putExtra("loadedImage", s);
                prefetchImage(s, i);
            }
        });
    }

    private void prefetchImage(final String imageUrl, final Intent intent) {
        RequestOptions requestOptions = new RequestOptions().skipMemoryCache(false).diskCacheStrategy(DiskCacheStrategy.DATA);

        Glide.with(getApplicationContext()).setDefaultRequestOptions(requestOptions).asBitmap().load(imageUrl).addListener(new RequestListener<Bitmap>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                showNotification(imageUrl, intent, resource);
                return true;
            }
        }).preload();
    }

    private void showNotification(String imageUrl, Intent intent, Bitmap bitmap) {
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext, CHANNEL_ID).setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Furry Animals")
                .setContentText("Prefetching a dog image...")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap))
                .setAutoCancel(true);

        final NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, imageUrl.hashCode(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        notificationManager.notify(666, builder.build());
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "cat";
            String description = "cat description";

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        disposable.dispose();
    }
}

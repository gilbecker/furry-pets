package com.wakaw.randomcat.data

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers


interface CatApiService {

    @GET("/v1/images/search?limit=20")
    @Headers("x-api-key: 03ade7fe-2df4-40b9-9bf8-9c163c96dd49")
    fun getRandomCat(): Call<List<FurryPet>>

    companion object {
        fun create(): CatApiService {

            val baseUrl = "https://api.thecatapi.com"

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseUrl)
                .build()

            return retrofit.create(CatApiService::class.java)
        }
    }
}
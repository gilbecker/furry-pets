package com.wakaw.randomcat.data

import com.wakaw.randomcat.R
import java.util.*

class SoundHelper {

    fun getRandomBark(): Int {

        val randomNumber = Random().nextInt(3)
        var sound: Int = R.raw.dog_sound1

        when (randomNumber) {
            1 -> sound = R.raw.dog_sound2
            2 -> sound = R.raw.dog_sound3
        }

        return sound    }

    fun getRandomMeow(): Int {
        val randomNumber = Random().nextInt(3)
        var sound: Int = R.raw.cat_sound1

        when (randomNumber) {
            1 -> sound = R.raw.cat_sound2
            2 -> sound = R.raw.cat_sound3
        }

        return sound
    }
}
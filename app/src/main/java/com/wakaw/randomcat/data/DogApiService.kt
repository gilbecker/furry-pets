package com.wakaw.randomcat.data

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers


interface DogApiService {

    @GET("/v1/images/search?limit=20")
    @Headers("x-api-key: 03ade7fe-2df4-40b9-9bf8-9c163c96dd49")
    fun getRandomDog(): Call<List<FurryPet>>

    companion object {

        val BASE_URL = "https://api.thedogapi.com"

        fun create(): DogApiService {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(Converters())
                .baseUrl(BASE_URL)
                .build()

            return retrofit.create(DogApiService::class.java)
        }
    }
}
package com.wakaw.randomcat.data

import android.arch.persistence.room.*


@Dao
//@TypeConverters(DataConverter::class)
interface FavoritePetsDao {

    @Query("SELECT url FROM furrypet")
    fun getAllFavoritePetsUrls(): List<String>

    @Query("SELECT * FROM furrypet")
    fun getAllFavoritePets(): List<FurryPet>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPet(furryPet: FurryPet?)

    @Delete
    fun deletePet(furryPet: FurryPet?)

    @Query("SELECT COUNT() FROM furrypet WHERE id = :id")
    fun isPetExists(id: String?): Int
}
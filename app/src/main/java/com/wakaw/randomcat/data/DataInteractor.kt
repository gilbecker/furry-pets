package com.wakaw.randomcat.data

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.util.Log
import io.reactivex.Observable
import retrofit2.Call

class DataInteractor(
    private val context: Context,
    private val catApi: CatApiService = CatApiService.create(),
    private val dogApi: DogApiService = DogApiService.create()
) {

    private var catsList = mutableListOf<FurryPet>()
    private var dogsList = mutableListOf<FurryPet>()

    private var currentCallsList = mutableListOf<Call<List<FurryPet>>>()

    private var lastPet: FurryPet? = null

    private val preferences = PreferenceManager.getDefaultSharedPreferences(context)

    fun cacheFirstBunchOfImages() {
        if (catsList.size == 0) {
            Thread {
                val response = catApi.getRandomCat().execute()

                if (response.isSuccessful) {
                    val resultsList = response.body()!!

                    for (result in resultsList) {
                        catsList.add(result)
                    }
                }
            }.start()
        }

        if (dogsList.size == 0) {
            Thread {
                val response = dogApi.getRandomDog().execute()

                if (response.isSuccessful) {
                    val resultsList = response.body()!!

                    for (result in resultsList) {
                        dogsList.add(result)
                    }
                }
            }.start()
        }
    }

    fun getRandomCatImage(): Observable<String> {

        when {
            catsList.size == 0 -> {
                return Observable.create { subscriber ->

                    val response = catApi.getRandomCat().execute()

                    if (response.isSuccessful) {
                        val resultList = response.body()!!

                        for (result in resultList) {
                            catsList.add(result)
                        }

                        subscriber.onNext(getAndRemoveCatImage())
                        subscriber.onComplete()
                    }
                }
            }
            catsList.size < 5 -> {
                return Observable.create { subscriber ->

                    subscriber.onNext(getAndRemoveCatImage())
                    subscriber.onComplete()

                    val response = catApi.getRandomCat().execute()

                    if (response.isSuccessful) {
                        val resultList = response.body()!!

                        for (result in resultList) {
                            catsList.add(result)
                        }
                    }
                }

            }
            else -> {
                return Observable.create { subscriber ->
                    subscriber.onNext(getAndRemoveCatImage())
                    subscriber.onComplete()
                }
            }

        }
    }

    private fun getAndRemoveCatImage(): String {
        val lastDog = catsList[0]
        lastPet = lastDog
        catsList.removeAt(0)
        return lastDog.url
    }

    fun getRandomDogSimple(): Observable<String> {
        return Observable.create { subscriber ->

            val response = dogApi.getRandomDog().execute()

            if (response.isSuccessful) {
                val resultList = response.body()!!

                for (result in resultList) {
                    dogsList.add(result)
                }

                subscriber.onNext(resultList.get(0).url)
                subscriber.onComplete()
            }
        }
    }

    fun getRandomDogImage(): Observable<String> {

        when {
            dogsList.size == 0 -> {
                return Observable.create { subscriber ->

                    val response = dogApi.getRandomDog().execute()

                    if (response.isSuccessful) {
                        val resultList = response.body()!!

                        for (result in resultList) {
                            dogsList.add(result)
                        }

                        subscriber.onNext(getAndRemoveDogImage())
                        subscriber.onComplete()
                    }
                }
            }
            dogsList.size < 5 -> {
                return Observable.create { subscriber ->

                    subscriber.onNext(getAndRemoveCatImage())
                    subscriber.onComplete()

                    val response = dogApi.getRandomDog().execute()

                    if (response.isSuccessful) {
                        val resultList = response.body()!!

                        for (result in resultList) {
                            dogsList.add(result)
                        }
                    }
                }

            }
            else -> {
                return Observable.create { subscriber ->
                    subscriber.onNext(getAndRemoveDogImage())
                    subscriber.onComplete()
                }
            }

        }
    }

    private fun getAndRemoveDogImage(): String {
        val lastDog = dogsList[0]
        lastPet = lastDog
        dogsList.removeAt(0)
        return lastDog.url
    }


    fun getRandomSound(isDog: Boolean): Int {
        return if (isDog) {
            SoundHelper().getRandomBark()
        } else {
            SoundHelper().getRandomMeow()
        }
    }

    fun insertPet() {
        Thread {
            if (lastPet != null) {
                LocalDatabase.getAppDataBase(context).favoritePetsDao().insertPet(lastPet)
            }
        }.start()
    }

    fun deletePet() {
        Thread {
            if (lastPet != null) {
                LocalDatabase.getAppDataBase(context).favoritePetsDao().deletePet(lastPet)
            }
        }.start()
    }

    fun isPetExists(): Observable<Boolean> {
        return Observable.create { subscriber ->

            val isPetExists = LocalDatabase.getAppDataBase(context).favoritePetsDao().isPetExists(lastPet?.id)

            subscriber.onNext(isPetExists > 0)
            Log.i("isPetExists = ", "" + isPetExists)
            subscriber.onComplete()
        }
    }

    fun getAllFavoritePets(): Observable<List<FurryPet>> {
        return Observable.create { subscriber ->
            val response = LocalDatabase.getAppDataBase(context).favoritePetsDao().getAllFavoritePets()

            subscriber.onNext(response)
            subscriber.onComplete()
        }
    }

    fun cancelRequests() {
        for (call in currentCallsList) {
            call.cancel()
            Log.i("cancelRequests", "Just canceled request: " + call.toString())
        }
    }

    fun saveLastImage() {
//        val sharedPref = context.getSharedPreferences("prefs", 0)
////        with (sharedPref.edit()) {
////            putString("lastUrl", lastPet?.url)
////            apply()
////        }

        preferences.edit().putString("lastUrl", lastPet?.url).apply()

    }

    fun getLastImage(): String? {
//        return context.getSharedPreferences("prefs", 0).getString("lastUrl", "")
        return preferences.getString("lastUrl", "https://www.rollingstone.com/wp-content/uploads/2018/09/shutterstock_9876703de.jpg")
    }
}
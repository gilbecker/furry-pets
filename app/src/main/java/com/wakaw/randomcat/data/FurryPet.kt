package com.wakaw.randomcat.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class FurryPet(
    @PrimaryKey val id: String,
    val url: String,
    val isDog: Boolean
)
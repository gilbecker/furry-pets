package com.wakaw.randomcat.data

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.Converter
import java.util.*

class Converters : Converter.Factory() {
    @TypeConverter
    fun fromBreedListToString(breeds: List<Breed>): String {
        return Gson().toJson(breeds).toString()
    }

    @TypeConverter
    fun stringToBreedList(breedString: String): List<Breed> {

        if (breedString.isEmpty()) {
            return Collections.emptyList()
        }

        val listTypeType = object : TypeToken<List<Breed>>() {}.type
        return Gson().fromJson(breedString, listTypeType)
    }
}

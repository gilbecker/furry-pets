package com.wakaw.randomcat.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context

@Database(entities = arrayOf(FurryPet::class), version = 1)
@TypeConverters(Converters::class)
abstract class LocalDatabase : RoomDatabase() {
    abstract fun favoritePetsDao(): FavoritePetsDao

    companion object {
        @Volatile
        private var INSTANCE: LocalDatabase? = null

        fun getAppDataBase(context: Context): LocalDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance =
                    Room.databaseBuilder(context.applicationContext, LocalDatabase::class.java, "localDB").build()
                INSTANCE = instance
                instance
            }
        }
    }
}
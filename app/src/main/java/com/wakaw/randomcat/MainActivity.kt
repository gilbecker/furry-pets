package com.wakaw.randomcat

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.wakaw.randomcat.animation.ButtonBounceInterpolator
import com.wakaw.randomcat.animation.CustomProgressBar
import com.wakaw.randomcat.animation.ImageBounceInterpolator
import com.wakaw.randomcat.data.DataInteractor
import com.wakaw.randomcat.favorites.FavoritePetsView
import kotlinx.android.synthetic.main.activity_main.*
import android.net.wifi.WifiManager


class MainActivity : AppCompatActivity(), MainContract.MainView {

    override fun changeLikeButtonState(result: Boolean?) {
        likeButton.visibility = View.VISIBLE
        likeButton.isChecked = result!!
    }

    private lateinit var presenter: MainPresenter
    private var lastImage = ""
    var isCelebrate: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        presenter = MainPresenter(this, DataInteractor(this))

        getCatButton.setOnClickListener {
            presenter.onCatButtonClicked(it)
        }

        getDogButton.setOnClickListener {
            presenter.onDogButtonClicked(it)
        }

        likeButton.setOnClickListener {
            presenter.onLikedClicked(it)

//            viewKonfetti.build()
//                .addColors(
//                    ContextCompat.getColor(applicationContext, R.color.yellow),
//                    ContextCompat.getColor(applicationContext, R.color.colorAccent),
//                    ContextCompat.getColor(applicationContext, R.color.colorPrimary),
//                    Color.GREEN
//                )
//                .setDirection(0.0, 359.0)
////                .setSpeed(pressure * 7, pressure * 14)
//                .setFadeOutEnabled(true)
//                .setTimeToLive(700)
//                .addShapes(Shape.RECT, Shape.CIRCLE)
//                .addSizes(Size(12, 5f))
//                .setPosition(likeButton.x, likeButton.y - 50f)
//                .burst(100)
        }

        mainImageView.setOnClickListener {
            presenter.onMainImageClicked(it)
        }

        if (isCelebrate) {
            presenter.celebrateCreation()
            isCelebrate = false
        }

        if (intent.hasExtra("loadedImage")) {
//            val imageUrl = intent?.extras?.getString("loadedImage")
            val imageUrl = intent.getStringExtra("loadedImage")
            showImage(imageUrl, true)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(ACCESS_COARSE_LOCATION), 666)
        } else {
            printNetworkSecurityType()
        }

//        if (savedInstanceState != null) {
//            presenter.getLastImage()
//        }

//        Stetho.initializeWithDefaults(this)

//        if (savedInstanceState != null) {
//            val imageRestored = savedInstanceState.getString("lastImage")
//
//            if (imageRestored != null) {
//                presenter.onViewRestored(imageRestored)
//            }
//        } else {
//            presenter.celebrateCreation()
//        }
    }


    override fun showImage(imageUrl: String?, isDog: Boolean) {
//        lastImage = imageUrl!!

        Glide.with(applicationContext).load(imageUrl)
            .apply(
                RequestOptions().placeholder(
                    CustomProgressBar(
                        this,
                        isDog
                    )
                ).diskCacheStrategy(DiskCacheStrategy.DATA)
            )
            .transition(withCrossFade())
            .into(mainImageView)
    }

    override fun clearImage() {
        likeButton.visibility = View.GONE
        Glide.with(applicationContext).clear(mainImageView)
    }

    override fun startButtonAnimation(view: View) {
        val animation = AnimationUtils.loadAnimation(this, R.anim.bounce)
        animation.interpolator = ButtonBounceInterpolator()
        view.startAnimation(animation)
    }

    override fun startImageAnimation(view: View) {
        val animation = AnimationUtils.loadAnimation(this, R.anim.bounce)
        animation.interpolator = ImageBounceInterpolator()
        view.startAnimation(animation)
    }

    override fun initConfetti() {
//        Timer().schedule(500) {
//            runOnUiThread {
//                viewKonfetti.build()
//                    .addColors(
//                        ContextCompat.getColor(applicationContext, R.color.yellow),
//                        ContextCompat.getColor(applicationContext, R.color.colorAccent),
//                        ContextCompat.getColor(applicationContext, R.color.colorPrimary),
//                        Color.GREEN
//                    )
//                    .setDirection(0.0, 359.0)
//                    .setSpeed(2f, 6f)
//                    .setFadeOutEnabled(true)
//                    .addSizes(Size(12))
//                    .setPosition(-50f, viewKonfetti.width + 50f, -50f, -50f)
//                    .streamFor(300, 2000L)
//            }
//        }
    }

    override fun playSound(sound: Int) {
        val assetFileDescriptor = resources.openRawResourceFd(sound) ?: return
        val mediaPlayer = MediaPlayerManager().requestPlayer() ?: return

        mediaPlayer.run {
            setDataSource(
                assetFileDescriptor.fileDescriptor, assetFileDescriptor.startOffset,
                assetFileDescriptor.declaredLength
            )
            prepareAsync()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.attachView(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.detachView()
    }

//    override fun onSaveInstanceState(outState: Bundle?) {
//        super.onSaveInstanceState(outState)
//
////        if (lastImage != "") {
////            outState?.putString("lastImage", lastImage)
////        }
//    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.mainmenu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.favorite -> {
            startActivity(Intent(this, FavoritePetsView::class.java))
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        printNetworkSecurityType()
    }

    private fun printNetworkSecurityType() {
        val wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

        val wifiScanReceiver = object : BroadcastReceiver() {
            override fun onReceive(c: Context, intent: Intent) {

                var success = false

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    success =
                        intent.getBooleanExtra(
                            WifiManager.EXTRA_RESULTS_UPDATED, false
                        )
                }

                if (success) {
                    scanSuccess(wifiManager)
                }
            }
        }

        val intentFilter = IntentFilter()
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)
        applicationContext.registerReceiver(wifiScanReceiver, intentFilter)

        wifiManager.startScan()
    }

    private fun scanSuccess(wifiManager: WifiManager) {
        val connectionInfo = wifiManager.connectionInfo
        val currentSSID = connectionInfo.ssid

        val networkList = wifiManager.scanResults

        for (network in networkList) {
            val capabilities = network.capabilities

            Log.d("NETWORK_STUFF", "Network: ${network.SSID}, Capabilities: $capabilities")

            if (currentSSID == network.SSID) {
                Log.i("NETWORK_STUFF", "Found network capabilities: $capabilities")
            }
        }
    }
}
